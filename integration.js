/**
 * Contains the shortest set of functions to interact with Cloud
 * Must be simple and strict. Please extract all extractable logic to other files 
 * because this will have very long test time.
 * 
 * covers internal dependency on the way of implementation, like puppeteer here.
 * 
 * each method must have try catch to isolate implementation specific issues from client
 * 
 * TODO need to rework and simplify instances() and create() operations
 */

const puppeteer = require('puppeteer');
const {PendingXHR} = require('pending-xhr-puppeteer');

class CloudAtCostIntegration {

    constructor(options) {
        let o = options || {};
    
        if (!o.username || !o.password) throw new Error('no credentials provided');
        this.username = o.username;
        this.password = o.password;

        this.width = 800;
        this.height = 600;
        this.started = false;
    }

    isStarted() {
        return this.started;
    }

    async stop() {
        try {
            await this.browser.close();
            this.started = false;
        } catch (err) {
            throw new Error('Failed to stop integration');
        }
    }

    async start() {
        try {
            this.browser = await puppeteer.launch({
                headless: true,
                //slowMo: 100,
                args: [`--window-size=${this.width},${this.height}`]
            });
            this.page = await this.browser.newPage();
            this.pendingXHR = new PendingXHR(this.page);
            await this.authenticate();
            this.started = true;
        } catch (err) {
            throw new Error('Failed to start integration');
        }
    }
  
    async authenticate() {
        await this.openPanelPage();
        await this.page.waitForSelector('#login-form');
        await this.page.type('input[name=username]', this.username);
        await this.page.type('input[name=password]', this.password);
        await this.page.click('input[name=submit');
        // TODO add handling of wrong credentials here
        await this.page.waitForSelector('a[href="logout.php"]');
    }
  
    //TODO clean up and simplify that method
    async instances() {
        await this.openPanelPage();
        await this.ensureInstacePanelsArePresent();
        let result = await this.page.$$eval('.main-holder .panel', panels => panels.map(panel => {

            //for Upgrade Now panel
            if (!panel.querySelectorAll('.panel-title td')[1]) {
                return null;
            }
  
            var docfrag = document.createDocumentFragment();
            var el = document.createElement('body');
            el.innerHTML = panel.querySelectorAll('.panel-title td')[1].querySelector('button').dataset['content'];
            for (var i = 0; 0 < el.childNodes.length;) {
                docfrag.appendChild(el.childNodes[i]);
            }
  
            var bodyRows = panel.querySelectorAll('.panel-body table table tr');
  
            let name = panel.querySelectorAll('.panel-title td')[0].innerText.trim();
            let ready = name.includes('Install') ? false : true;
            let active = !ready ? false
                : panel.querySelectorAll('.panel-title td')[1].querySelectorAll('ul.dropdown-menu li')[1].classList.contains('disabled') ? true : false;
  
            return {
                name: name,
                ready: ready,
                active: active,
                serverId: docfrag.querySelectorAll('tr')[0].querySelectorAll('td')[1].innerText.trim(),
                installed: docfrag.querySelectorAll('tr')[1].querySelectorAll('td')[1].innerText.trim(),
                ipAddress: docfrag.querySelectorAll('tr')[2].querySelectorAll('td')[1].innerText.trim(),
                netmask: docfrag.querySelectorAll('tr')[3].querySelectorAll('td')[1].innerText.trim(),
                gateway: docfrag.querySelectorAll('tr')[4].querySelectorAll('td')[1].innerText.trim(),
                password: docfrag.querySelectorAll('tr')[5].querySelectorAll('td')[1].innerText.trim(),                
                currentOs: bodyRows[0].querySelectorAll('td')[1].innerText.trim(),
                ipv4: bodyRows[1].querySelectorAll('td')[1].innerText.trim(),
                ipv6: bodyRows[2].querySelectorAll('td')[1].innerText.trim(),
                hostname: bodyRows[3].querySelectorAll('td')[1].innerText.trim(),
                cpu: parseInt(bodyRows[4].querySelectorAll('td')[0].innerText.trim().replace(' CPU:', '')),
                ram: bodyRows[5].querySelectorAll('td')[0].innerText.trim().replace(' RAM:', ''),
                ssd: bodyRows[6].querySelectorAll('td')[0].innerText.trim().replace(' SSD:', ''),
            };
        }));
        
        return result.filter(val => val != null);
    }
    
    async create(options = {}, progressListener = () => {}) {
        await this.openPanelPage();
        await this.page.click('.header-menu a[href=\'#\']'); //click first button in header
  
        try {
            progressListener('connecting...');
            await this.page.waitForSelector('#queModal #modalBody button');
            await this.page.click('#queModal #modalBody button');
            await this.page.waitForSelector('#modalBody form');

            progressListener('configuring...');
            await this.selectOptionByText('datacenter', options.dc);
            await this.selectOptionByText('os', options.os);
            await this.selectOptionByText('cpu', options.cpu);
            await this.selectOptionByText('ram', options.ram);
            await this.selectOptionByText('storage', options.storage);      
  
            progressListener('submitting configuration...');
            await this.page.click('#modalBody form button[type="submit"]');
            
            await this.ensureInstacePanelsArePresent();
            return await this.trackInstallationProcess(progressListener);
        } catch (err) {
            //TODO extract to separate method process of scrapping of error description
            await this.waitForDialogPopsUp();
            const text = await this.page.$eval('#modalBody', 
                el => el.innerText.split('\n\n')[0].trim().replace('&nbsp;',''));
            throw new Error(text);
        }
    }

    async delete(instanceId) {
        await this.openPanelPage();
        await this.clickItemInModifyDropdown(instanceId, 'Delete Server');
        await this.clickButtonInQuestionDialog('Delete');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    async rename(instanceId, name) {
        await this.openPanelPage();
        await this.clickItemInModifyDropdown(instanceId, 'Rename Server');
        await this.waitForDialogPopsUp();
        await this.page.type('input#RENserver', name);
        await this.clickButtonInQuestionDialog('Continue');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    //TODO has a case when domain is invalid
    async setReverseDNS(instanceId, hostname) {
        await this.openPanelPage();    
        await this.clickItemInModifyDropdown(instanceId, 'Reverse DNS');
        await this.waitForDialogPopsUp();
        await this.page.type('input#inputRDNS', hostname);
        await this.clickButtonInQuestionDialog('Continue');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    async reboot(instanceId) {
        await this.openPanelPage();
        await this.clickItemInPowerDropdown(instanceId, 'Reboot');
        await this.clickButtonInQuestionDialog('Yes');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    async poweron(instanceId) {
        await this.openPanelPage();
        await this.clickItemInPowerDropdown(instanceId, 'Power On');
        await this.clickButtonInQuestionDialog('Yes');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    async poweroff(instanceId) {
        await this.openPanelPage();
        await this.clickItemInPowerDropdown(instanceId, 'Power Off');
        await this.clickButtonInQuestionDialog('Yes');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    /*==============================================================================*/
  
    async openPanelPage() {
        await this.page.goto('https://panel.cloudatcost.com/index.php', {waitUntil: 'networkidle0'});
    }

    async clickItemInPowerDropdown(instanceId, itemTitle) {
        await this.page.click(`#PowerDrp_${instanceId}`);
        await this.page.$$eval(`#PowerDrp_${instanceId} + ul li a`, (a, itemTitle) => {
            a.filter((el) => el.innerText.includes(itemTitle))[0].click();
        }, itemTitle);
    }

    async clickItemInModifyDropdown(instanceId, itemTitle) {
        await this.page.click(`#ModifyDrp_${instanceId}`);
        await this.page.$$eval(`#ModifyDrp_${instanceId} + ul li a`, (a, itemTitle) => {
            a.filter((el) => el.innerText.includes(itemTitle))[0].click();
        }, itemTitle);
    }

    async selectOptionByText(fieldName, text) {
        let simplified = (text || '').toString().replace(/\s/g,'').toLowerCase();
        let value = await this.page.$$eval(`select[name="${fieldName}"] option`, (o, fieldName, simplified) => {
            try {
                return o.filter(el => el.text.replace(/\s/g,'').toLowerCase().includes(simplified))[0].getAttribute('value');
            } catch (e) {        
                throw new Error(`No such ${fieldName} value available`);
            }
        }, fieldName, simplified);
        await this.page.select(`select[name="${fieldName}"]`, value);
    }

    async clickButtonInQuestionDialog(buttonText) {        
        await this.waitForDialogPopsUp();
        await this.page.$$eval('#btns button', (btns, text) => {
            btns.filter(el => el.innerText.includes(text))[0].click();
        }, buttonText);
    }

    //wait until panels present or throw error otherwise
    async ensureInstacePanelsArePresent() {
        await this.page.waitForSelector('.main-holder .panel');
        await this.pendingXHR.waitForAllXhrFinished();
    }

    async waitForDialogPopsUp() {
        await this.page.waitForSelector('#queModal');
    }

    async trackInstallationProcess(progressListener = () => {}) {
        let info;
        while (true) {
            try {
                const insts = await this.instances();
                info = insts[0];
                if (insts[0].ready) break;
                let str = insts[0].name.replace(/(Status)?(\n)?/g, '').trim();
                let status = str.replace(/(InstallingSweet!!)?(Progress)?(%)?(\d)?/g, '').replace('....','...').trim().toLowerCase();
                let percentage = parseInt(str.replace(/\D/g, ''), 10) || undefined;
                progressListener(status, percentage);
                await this.page.waitFor(1000);
            } catch (e) {
                console.log('fail to fetch status');
                console.log(e);
                await this.page.waitFor(1000);
            }
        }
        return info;
    }

}

module.exports = CloudAtCostIntegration;
