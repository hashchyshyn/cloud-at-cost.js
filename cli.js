#!/usr/bin/env node

/**
 * This file is responsible for command line input parsing and results visualizing
 */

const program = require('commander');
const {table} = require('table');
const chalk = require('chalk');
const cliProgress = require('cli-progress');
const fs = require('fs');
const YAML = require('yaml');
const CloudAtCostClient = require('./client');

program
    .version('0.1.0', '-v, --version')
    .option('-s, --silent', 'Run without step by step comments');

program
    .command('login <username> <password>')
    .action(function (username, password) {
        (async() => {
            let client = new CloudAtCostClient();
            await client.login(username, password);
            await client.close();
        })();
    });

program
    .command('whoami')
    .action(function() {
        (async () => {
            let client = new CloudAtCostClient();
            let username = await client.whoami();            
            console.log(username ? username : 'not set');
            await client.close();
        })();        
    });

program
    .command('logout')
    .action(function() {
        (async () => {
            let client = new CloudAtCostClient();
            await client.logout();
            await client.close();
        })();
    });

program
    .command('list')
    .option('-f, --format', 'Output format, like csv, table or json')
    .action(function () {
        (async() => {
            try {
                let client = new CloudAtCostClient();
                let instances = await client.instances();
                await client.close();

                let data = [];
                data.push([chalk.blue('State'), chalk.blue('Server ID'), chalk.blue('IP Address'), chalk.blue('Name'), 
                    chalk.blue('Current OS'), chalk.blue('CPU'), chalk.blue('RAM'), chalk.blue('SSD')]);
                for (let i=0; i < instances.length; i++) {
                    let ins = instances[i];

                    let active = ins.active && ins.ready;
                    let activeCell = active ? chalk.bgGreenBright.bold(' Running ') : chalk.bgRedBright.bold(' Stopped ');

                    data.push([
                        activeCell,
                        ins.serverId,
                        ins.ipAddress,
                        ins.name,
                        ins.currentOs,
                        ins.cpu,
                        ins.ram,
                        ins.ssd
                    ]);
                }                
                console.log(table(data));
            } catch (err) {
                console.error(chalk.redBright(err.message));
            }
        })();
    });

program
    .command('create [files...]')    
    
    .option('--name <name>')
    .option('--dc <dc>')
    .option('--os <os>')
    .option('--cpu <cpu>')
    .option('--ram <ram>')
    .option('--storage <storage>')
    .option('--hostname <hostname>')

    .option('--username <username>', 'Specifies username override')
    .option('--password <password>', 'Specifies password override')

    .action(function(files, modifiers) {
        (async() => {
            let fileConfigs = readConfigurationsFromFiles(files);
            if (fileConfigs.length == 0) fileConfigs.push({});
            let configurations = applyModifiers(fileConfigs, modifiers);
            
            let client;
            try {
                if (modifiers.username && modifiers.password) {
                    let credentials = {username: modifiers.username, password: modifiers.password};
                    client = new CloudAtCostClient(credentials);
                } else {
                    client = new CloudAtCostClient();
                }
                
                let bar;
                let info = await client.create(configurations, (status, percentage) => {
                    if (!bar && percentage !== undefined) {
                        process.stdout.write('\r');
                        bar = new cliProgress.Bar({
                            format: '{status} {bar} {percentage}%'
                        }, cliProgress.Presets.shades_classic);
                        bar.start(100, 0);
                    }
                    if (bar && !percentage) {
                        //print('finalizing...');
                    }
                    if (bar) {
                        bar.update(percentage, {status});
                    } else {
                        print(status);
                    }
                });

                //here output some info about recently created instance
                print('');
                if (info) {
                    console.log(info);
                    //console.log(JSON.stringify(info));
                    console.log();
                }
                
                if (bar) await bar.stop();            
                await client.close();
            } catch (err) {
                console.error(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }

        })();
    });


function readConfigurationsFromFiles(files) {
    let configurations = [];
    if (files && files.length) {
        files.forEach(path => {
            let file = fs.readFileSync(path, 'utf8');
            if (path.endsWith('.yml') || path.endsWith('.yaml')) {
                configurations.push(YAML.parse(file));
            } else if (path.endsWith('.json')) {
                configurations.push(JSON.parse(file));
            } else {
                throw new Error('Unsupported configuration file format');
            }
        });
    }
    return configurations;
}

function applyModifiers(configurations, modifiers) {
    configurations.forEach(c => {
        if (!(modifiers.name instanceof Function)) c.name = modifiers.name;
        if (modifiers.dc) c.dc = modifiers.dc;
        if (modifiers.os) c.os = modifiers.os;
        if (modifiers.cpu) c.cpu = modifiers.cpu;
        if (modifiers.ram) c.ram = modifiers.ram;
        if (modifiers.storage) c.storage = modifiers.storage;
        if (modifiers.hostname) c.hostname = modifiers.hostname;
    });
    return configurations;
}

program
    .command('delete <instance>')    
    .action(function(instance) {
        (async() => {
            let client;
            try {
                client = new CloudAtCostClient();
                await client.delete(instance);                        
                await client.close();
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });

program
    .command('poweron <instance>')
    .action(function(instance) {
        (async() => {
            let client;
            try {                
                client = new CloudAtCostClient();                            
                await client.powerOn(instance);
                await client.close();                
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });

program
    .command('poweroff <instance>')
    .action(function(instance) {
        (async() => {
            let client;
            try {
                client = new CloudAtCostClient();                
                await client.powerOff(instance);
                await client.close();
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });

program
    .command('reboot <instance>')
    .action(function(instance) {
        (async() => {
            let client;
            try {
                client = new CloudAtCostClient();                
                await client.reboot(instance);
                await client.close();                
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });

program
    .command('reversedns <instance> <hostname>')
    .action(function(instance, hostname) {
        (async() => {
            let client;
            try {
                client = new CloudAtCostClient();                            
                await client.setReverseDns(instance, hostname);
                await client.close();                
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });

program
    .command('rename <instance> <name>')
    .action(function(instance, name) {
        (async() => {
            let client;
            try {
                client = new CloudAtCostClient();                            
                await client.rename(instance, name);
                await client.close();                
            } catch (err) {
                console.log(err);
                if (typeof client !== 'undefined' && client !== null) {
                    await client.close();
                }
            }
        })();
    });


program.parse(process.argv);


function print(message) {
    process.stdout.write('\r                                                                                ');
    process.stdout.write('\r' + message);
}