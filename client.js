/**
 * Contains many rich methods for interacting with Cloud than integration
 * 
 * Should it be closable? - yes #to save time in call sequences
 * It incapsulates credential management # it does know where and in which priority to fetch credetials from
 * Incapsulates work with files and formats of configurations # it require and handle all format aspects
 */

const Preferences = require('preferences');

const CloudAtCostIntegration = require('./integration');

module.exports = class CloudAtCostClient {

    constructor(credentials) {
        this.prefs = new Preferences('cloudatcost.app', {});
        const username = process.env.CLOUDATCOST_USERNAME;
        const password = process.env.CLOUDATCOST_PASSWORD;        
        if (credentials) {
            this.integration = new CloudAtCostIntegration(credentials);
        } else if (this.prefs['credentials']) {
            this.integration = new CloudAtCostIntegration(this.prefs['credentials']);
        } else if (username && password) {
            this.integration = new CloudAtCostIntegration({username, password});
        } else {
            throw new Error('No credentials provided');
        }
    }

    async close() {
        try {
            if (typeof this.integration !== 'undefined' && this.integration !== null) {
                await this.integration.stop();
            }
        } catch (e) {}
    }

    //lazy start
    async ensureIntegrationStarted() {
        if (!this.integration.isStarted()) {        
            await this.integration.start();        
        }
    }

    async instances() {
        await this.ensureIntegrationStarted();
        return await this.integration.instances();
    }

    async create(confs = [], progressCb = () => {}) {        
        await this.ensureIntegrationStarted();
        let result = [];
        for (let i = 0; i < confs.length; i++) {
            let configuration = confs[i];
            let instanceDescription = await this.integration.create(configuration, progressCb);
            progressCb('Applying name, reverse dns...');
            let id = instanceDescription.serverId;
            if (configuration.name) {
                await this.integration.rename(id, configuration.name);
                instanceDescription.name = configuration.name;
            }            
            if (configuration.hostname) {
                await this.integration.setReverseDNS(id, configuration.hostname);
                instanceDescription.hostname = configuration.hostname;
            }
            result.push(instanceDescription);
        }
        return result;
    }    

    async login(username, password) {
        let cloud = new CloudAtCostIntegration({
            username: username,
            password: password
        });
        try {
            await cloud.start();
            await cloud.stop();
            this.prefs['credentials'] = {
                username: username,
                password: password
            };
        } catch (err) {
            throw new Error('rejected');
        }
    }

    async whoami() {
        return this.prefs['credentials'].username;
    }

    async logout() {
        this.prefs['credentials'] = {};
    }

    async delete(instanceId) {
        await this.ensureIntegrationStarted();
        let ins = await this.integration.instances();
        if (ins.findIndex(o => o.serverId == instanceId) != -1) {
            await this.integration.delete(instanceId);
        } else {
            throw new Error('Instance not found');
        }
    }    

    async setReverseDns(instanceId, value) {
        await this.ensureIntegrationStarted();
        await this.integration.setReverseDNS(instanceId, value);
    }

    async rename(instanceId, value) {
        await this.ensureIntegrationStarted();
        await this.integration.rename(instanceId, value);
    }

    async powerOn(instanceId) {
        await this.ensureIntegrationStarted();
        await this.integration.poweron(instanceId);
    }

    async powerOff(instanceId) {
        await this.ensureIntegrationStarted();
        await this.integration.poweroff(instanceId);
    }

    async reboot(instanceId) {
        await this.ensureIntegrationStarted();
        await this.integration.reboot(instanceId);
    }


};

