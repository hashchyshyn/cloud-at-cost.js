### Description

It is javascript library and based on it command line tool. 
Library part is designed to be used in a context of larger constructions.
CLI is convinient tool to write automation scripts for provisioning cloud-at-cost instances.

Library itself is based on an assumption of having correct integration backend. That backend might be
any implementation, however while native cloud-at-cost API is not available I've built a polifill implementation
based on puppeteer. Even while it is SLOW, it works, and can be used as a substituion until native API will be published.
Upon API publication, it can be created another implementation of integration layer, 
while all library consumers will remain untouched.

### Motivation

Cloud At Cost has no API available/running.

Unless this ticket is not fixed, this tool may be used as a substitution for a while.
https://github.com/cloudatcost/api/issues/52

### Usage

To use CLI tool globally install it with correspondent parameter
`npm -g i cloud-at-cost.js`

Then it will be available in console by names `cloud`, `cloudatcost` and `cloud-at-cost`.
Several names are used for manual and scripting purposes where short and descriptive names desired respectively.

### Authentication

There are three ways to do authentication:

 - environment variables
 - login, whoami, logout commands
 - --username --password parameters

Environment variables might be the easiest way to setup credentials in server environment. 
Variable names are:
`CLOUDATCOST_USERNAME`
`CLOUDATCOST_PASSWORD`

For user sessions it may be more convinient to use `login`/`logout` commands. Credentials will be saved
in a plain file in your user folder. By using `whoami` command you may check current set user.

In case you don't want to disclose your credentials and save it anywhere you may pass them into each command
using `--username` and `--password` parameters. BTW, this is the most prioritized way, so it will overlay both 
environment variables and `login`/`logout` approaches.

### Listing existing instances

In CLI tool `list` command will print a table with some important properties of the instances.
NOTE: root passwords are not displayed in this table by default.

   PS C:\git\cloud-at-cost.js> cloud list
   ╔═══════════╤═══════════╤════════════════╤═══════════════════╤══════════╤══════════════════╤═════╤═══════╤══════╗
   ║ State     │ Server ID │ IP Address     │ Name              │ Run Mode │ Current OS       │ CPU │ RAM   │ SSD  ║
   ╟───────────┼───────────┼────────────────┼───────────────────┼──────────┼──────────────────┼─────┼───────┼──────╢
   ║  Running  │ 255163447 │ 104.147.97.22  │ specialA          │ Normal   │ CentOS 6.9 64bit │ 1   │ 512MB │ 10GB ║
   ╟───────────┼───────────┼────────────────┼───────────────────┼──────────┼──────────────────┼─────┼───────┼──────╢
   ║  Running  │ 255163445 │ 104.147.99.143 │ testInstanceConf1 │ Normal   │ CentOS 6.9 64bit │ 1   │ 512MB │ 10GB ║
   ╟───────────┼───────────┼────────────────┼───────────────────┼──────────┼──────────────────┼─────┼───────┼──────╢
   ║  Running  │ 255160836 │ 45.62.223.83   │ MQDevelopment     │ Normal   │ CentOS 7 64bit   │ 1   │ 512MB │ 10GB ║
   ╟───────────┼───────────┼────────────────┼───────────────────┼──────────┼──────────────────┼─────┼───────┼──────╢
   ║  Running  │ 255157662 │ 45.62.219.48   │ DBDev             │ Normal   │ CentOS 7 64bit   │ 1   │ 512MB │ 10GB ║
   ╟───────────┼───────────┼────────────────┼───────────────────┼──────────┼──────────────────┼─────┼───────┼──────╢
   ║  Running  │ 255156956 │ 45.62.237.206  │ GameServer        │ Normal   │ CentOS 7 64bit   │ 1   │ 512MB │ 10GB ║
   ╚═══════════╧═══════════╧════════════════╧═══════════════════╧══════════╧══════════════════╧═════╧═══════╧══════╝

### Creating new instances

In CLI tool `create` command will create new instance, if possible, according to configurations and parameters specified.
As a result of the command execution JSON representation of created instance metadata will be returned.

NOTE: currently installation process takes about 5-10 minutes, so don't be surprised. To make it a bit more responsive, CLI tool
will provide current step status with percentage.

By running simple `cloud create` you will get the instance with defaults parameters, which are specific to each account. It the same
like if you will click build button in panel.cloudatcost.com and then run through withou any changes.

    PS C:\git\cloud-at-cost.js> cloud create
    [ { name: 'c999960556-cloudpro-637419866',
        ready: true,
        active: true,
        serverId: '255463433',
        installed: '04/03/2019',
        ipAddress: '104.167.93.151',
        netmask: '255.255.255.0',
        gateway: '104.167.93.1',
        password: 'AbugALxLsq',
        runMode: 'Safe',
        currentOs: 'CentOS 6.7 64bit',
        ipv4: '104.167.93.151',
        ipv6: 'Activate',
        hostname: 'Not Assigned',
        cpu: 1,
        ram: '512MB',
        ssd: '10GB' 
    } ]

You can specify parameters of the instance to be created by passing a configutation file. YAML and JSON formats accepted.

   PS C:\git\cloud-at-cost.js> cloud create test-config.yml

Multiple files are also supported. It will be longer but it might be useful to build set of instances in on shot.

   PS C:\git\cloud-at-cost.js> cloud create test-config.yml other-test-config.json

And on top of it there are full set of modifiers parameters like `--cpu`, `--name` or `--runmode` may be applied as well. 
In case of multiple configuration run modifiers will be applied to all configurations. 
In case of no configurations provided it will be instance created only based on defaults and modifiers.
