Roadmap
-------

1. Write authentication.md about how authentication works, all 3 flows
2. Write layers.md about layering principle. Integration, client library and command-line
3. Reassess the most important use cases for the tool and mention them in README.md
4. Create tests for these most important use cases
