const CloudAtCostIntegration = require('./integration');
const username = process.env.CLOUDATCOST_USERNAME;
const password = process.env.CLOUDATCOST_PASSWORD;

const ipv4Pattern = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

let cloud;

beforeAll(async () => {
    cloud = new CloudAtCostIntegration({ username, password });
    await cloud.start();
}, 15 * 1000);

afterAll(async () => {
    if (cloud) {
        await cloud.stop();
    }
});

describe('list', () => {
    it('returns array of objects with expected structure', async () => {
        let instances = await cloud.instances();
        expect(instances instanceof Array).toBeTruthy();

        instances.forEach((instance) => {
            // Ensure each instance is an object with an exact set of keys
            expect(typeof instance).toEqual('object');
            expect(Object.keys(instance)).toEqual([
                'name',
                'ready',
                'active',
                'serverId',
                'installed',
                'ipAddress',
                'netmask',
                'gateway',
                'password',                
                'currentOs',
                'ipv4',
                'ipv6',
                'hostname',
                'cpu',
                'ram',
                'ssd',
            ]);
       
            // Validate property types
            expect(typeof instance.name).toEqual('string');
            expect(typeof instance.ready).toEqual('boolean');
            expect(typeof instance.active).toEqual('boolean');
            expect(typeof instance.serverId).toEqual('string');
            expect(typeof instance.installed).toEqual('string');
            expect(typeof instance.ipAddress).toEqual('string');
            expect(typeof instance.netmask).toEqual('string');
            expect(typeof instance.gateway).toEqual('string');
            expect(typeof instance.password).toEqual('string');            
            expect(typeof instance.currentOs).toEqual('string');
            expect(typeof instance.ipv4).toEqual('string');
            expect(typeof instance.ipv6).toEqual('string');
            expect(typeof instance.hostname).toEqual('string');
            expect(typeof instance.cpu).toEqual('number');
            expect(typeof instance.ram).toEqual('string');
            expect(typeof instance.ssd).toEqual('string');

            expect(instance.ipAddress).toMatch(ipv4Pattern);
            expect(instance.ipv4).toMatch(ipv4Pattern);
            expect(instance.netmask).toMatch(ipv4Pattern);
            expect(instance.gateway).toMatch(ipv4Pattern);            
        });        


    }, 60 * 1000);

});

describe('create', () => {
    it('creates default instance when no parameters specified', async () => {
        let created = await cloud.create();
        expect(true).toBe(true);
    }, 15 * 60 * 1000);
});

describe('rename', () => {
    it('changes name for instance specified by id', async () => {
        let newName = 'testname' + Date.now();        
        let instanceBefore = (await cloud.instances())[0];
        expect(instanceBefore.name).not.toBe(newName);
        await cloud.rename(instanceBefore.serverId, newName);
        let instanceAfter = (await cloud.instances())[0];
        expect(instanceAfter.name).toBe(newName);
    }, 1 * 60 * 1000);
});

describe('reverse DNS', () => {
    it('changes DNS name for instance specified by id', async () => {
        let newDnsName = 'a' + Date.now() + '.test.com';
        let instanceBefore = (await cloud.instances())[0];
        expect(instanceBefore.hostname).not.toBe(newDnsName);
        await cloud.setReverseDNS(instanceBefore.serverId, newDnsName);
        let instanceAfter = (await cloud.instances())[0];
        expect(instanceAfter.hostname).toBe(newDnsName);
    }, 1 * 60 * 1000);
});

describe('reboot', () => {
    it('reboots instance specified by id', async () => {
        let instanceBefore = (await cloud.instances())[0];
        expect(instanceBefore.active).toBe(true);
        await cloud.reboot(instanceBefore.serverId);
        let instanceAfter = (await cloud.instances())[0];
        expect(instanceAfter.active).toBe(true);
    }, 5 * 60 * 1000);
});

describe('poweroff', () => {
    it('turns off instance specified by id', async () => {
        let instanceBefore = (await cloud.instances())[0];
        expect(instanceBefore.active).toBe(true);
        await cloud.poweroff(instanceBefore.serverId);
        let instanceAfter = (await cloud.instances())[0];
        expect(instanceAfter.active).toBe(false);

        await cloud.poweron(instanceBefore.serverId);
    }, 5 * 60 * 1000);
});

describe('poweron', () => {
    it('turns on instance specified by id', async () => {
        let instancePre = (await cloud.instances())[0];
        await cloud.poweroff(instancePre.serverId);

        let instanceBefore = (await cloud.instances())[0];
        expect(instanceBefore.active).toBe(false);
        await cloud.poweron(instanceBefore.serverId);
        let instanceAfter = (await cloud.instances())[0];
        expect(instanceAfter.active).toBe(true);
    }, 5 * 60 * 1000);
});

describe('delete', () => {
    it('deletes instance specified by id', async () => {
        let instanceBefore = await cloud.instances();
        let serverId = instanceBefore[0].serverId;
        let instancesAmount = instanceBefore.length;
        
        if (instanceBefore[0].name.includes('testname')) {
            await cloud.delete(serverId);
        }
        let instancesAfter = await cloud.instances();

        expect(instancesAfter.length).toEqual(instancesAmount - 1);
        expect(instancesAfter[0].serverId).not.toEqual(serverId);
    }, 3 * 60 * 1000);
});
