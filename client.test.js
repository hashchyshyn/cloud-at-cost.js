const CloudAtCostClient = require('./client');

jest.mock('./integration', () => {
    /**
     * This mock describes what interface Client demands from integration level
     */    
    return class CloudAtCostIntegration {
        constructor(credentials) {
            this.fakeInstanceList = [{serverId: 'ins500'}];
        }
        isStarted() {return true;}
        async start() {}
        async stop() {}
        async instances() { return this.fakeInstanceList; }
        async create() { 
            let o = {serverId: 5};
            this.fakeInstanceList.push(o);
            return o; }
        async delete(id) {
            const index = this.fakeInstanceList.indexOf({serverId: id});
            this.fakeInstanceList.splice(index, 1);
        }
        async rename() {}
        async setReverseDNS(instanceId, hostname) {}
        async setRunMode() {}
        async reboot() {}
        async poweron(instanceId) {}
        async poweroff(instanceId) {}
    };
});

describe('client.instances() method', () => {
    it('returns array of objects', async () => {
        const client = new CloudAtCostClient();
        let result = await client.instances();
        expect(result instanceof Array).toBeTruthy();
    });
});

// describe('client.create() method', () => {
//     it('no parameters yelds creation of instance with default config', async () => {
//         const client = new CloudAtCostClient();
//         let result = await client.create();
//         expect(result[0]).toEqual({serverId: 5});
//     }); 
// });

describe('client.delete(instanceId) method', () => {
    it('simply deletes referenced instance', async () => {
        const client = new CloudAtCostClient();
        await client.delete('ins500');
    });
    it('throws an error when there is no such instance', async () => {
        const client = new CloudAtCostClient();
        await expect(client.delete('ins_non_existing')).rejects.toThrow('Instance not found');
    });

});
